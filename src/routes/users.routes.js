const express = require('express');
const router = express.Router();

const User = require('../models/User.js');

router.get('/users', async(req, res) => {
    const users = await User.find();
    console.log(users);
    res.json(users);
});

router.get('/users/:id/find', async(req, res) => {
    const user = await User.findById(req.params.id);
    res.json(user);
});

router.post('/users/create', async(req, res) => {
    const {name, email, password} = req.body;
    const user = new User({name, email, password});
    await user.save();
    res.json({status: 'User saved !'});
});

router.put('/users/:id/update', async(req, res) => {
    const {name, email, password} = req.body;
    const newUser = {name, email, password};
    await User.findByIdAndUpdate(req.params.id, newUser);
    console.log(req.params.id);
    res.json({status: 'User updated !'});
});

router.delete('/users/:id/delete', async(req, res) => {
    await User.findByIdAndRemove(req.params.id);
    res.json({status: 'User removed !'});
});


module.exports = router;