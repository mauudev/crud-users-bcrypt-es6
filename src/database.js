const mongoose = require('mongoose');
const URI = 'mongodb://localhost/users-challenge';

mongoose.connect(URI)
        .then(db => console.log('DB is connected !'))
        .catch(err => console.error(err));  
            
mongoose.Promise = global.Promise;

module.exports = mongoose;