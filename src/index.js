const express = require('express');
const morgan = require('morgan');
const path = require('path');
const bodyParser = require('body-parser');
const { mongoose } = require('./database');

const app = express();

//Settings
app.set('port', process.env.PORT || 3000); // esto automaticamente toma el puerto del server 
//o en otro caso el puerto 3000


// Set body parser for parse incoming requests bodies in a middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//Middlewares
app.use(morgan('dev'));
app.use(express.json());

//Routes  
app.use('/', require('./routes/users.routes'));


//Static files
app.use(express.static(path.join(__dirname, 'public'))); 

//Starting the server
app.listen(app.get('port'), () =>{ 
    console.log('Server on port '+app.get('port'));
});